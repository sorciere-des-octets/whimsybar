# Whimsybar
![example](screenshot.png)
My i3 status bar.

For best results, use a dark GTK theme

## Usage
1. Add the following to i3 config file:
```bash
exec_always killall eww; eww open status-bar; eww open status-bar-secondary;
```
_status-bar-secondary_ is not necessary if there is only one monitor.

2. Create a symlink in the .config folder
```bash
ln -s <path-to-whimsybar> ~/.config/eww
```

## Dependencies
- [eww](https://github.com/elkowar/eww)
- [dasel](https://github.com/TomWright/dasel)
- [i3](https://i3wm.org/)

