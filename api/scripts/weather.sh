#!/bin/sh
if [ -f data/weather.txt ] && [ $(head -1 data/weather.txt) -ge $(date +"%s") ]; then
  # Do nothing, take value from file
  break
else
  TEMP=$(curl wttr.in/?format=1)
  date -d "today 60 minutes" +"%s" > data/weather.txt
  echo "$TEMP" >> data/weather.txt
fi

echo $(tail -1 data/weather.txt)