#!/bin/sh
output=$1

while true; do
  ws_state=$(i3-msg -t get_workspaces | dasel -r json "all().filter(equal(output,${output:-"DP-0"})).mapOf(num,num,name,name,focused,focused,output,output).merge()")
  echo $ws_state
  ws_change=$(i3-msg -t subscribe '[ "workspace" ]')
done