#!/bin/sh
#TODO use BEGIN block in awk instead of sed
csv_headers='id,name,type'
csv_rows=$(xrandr --listactivemonitors | awk '/[0-9]:/ {print substr($1, 0, 1)" "$2}' | sed 's/+//g; /*/ s/$/ primary/; /*/! s/$/ secondary/g;  s/*//; s/ /,/g')
json_data=$(echo -e "$csv_headers\n$csv_rows" | dasel -r csv -w json)

primary_query="all().filter(equal(type,primary))" # As object  
secondary_query="all().filter(equal(type,secondary)).merge()" # As array
   
primary_display=$(echo -e "$json_data" | dasel -r json "$primary_query")
secondary_display=$(echo -e "$json_data" | dasel -r json "$secondary_query")
echo -e "$primary_display"
eww update primary-display="$primary_display"
