#!/bin/sh
output=$1
workspaces=$(i3-msg -t get_config)

if [[ "$output" == "csv" || "$output" == "json" ]]
then
  workspaces=$(echo "$workspaces" | awk 'BEGIN{print "num,name"}; /^set \$ws/ {print substr($2, 4)"," $3}')
else
  workspaces=$(echo "$workspaces" | awk '/^set \$ws/ {print substr($2, 4)" "$3}')
fi

if [[ "$output" == "json" ]]
then
  echo "$workspaces" | dasel -r csv -w json
else
  echo "$workspaces"
fi
